prod(deadmau5).
prod(martin_garriax).
prod(hardwell).
prod(david_guetta).
prod(tiesto).
prod(don_diablo).
prod(afrojack).
prod(oliver_heldens).
prod(marshmello).
prod(steve_aoki).
prod(rehab).
prod(avicii).
prod(kshmr).
prod(skrillex).
prod(kygo).

genre(house, deadmau5).
genre(house, martin_garriax).
genre(house, don_diablo).
genre(house, avicii).
genre(house, rehab).
genre(house, kshmr).
genre(house, kygo).
genre(house, afrojack).
genre(house, tiesto).

genre(big_room, martin_garriax).
genre(big_room, oliver_heldens).
genre(big_room, kshmr).
genre(big_room, hardwell).
genre(big_room, don_diablo).

genre(trap, marshmello).
genre(trap, steve_aoki).
genre(trap, skrillex).

genre(dubstep, skrillex).
genre(elec_pop, david_guetta).