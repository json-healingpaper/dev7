```
$ swipl
```

```
?- ['friends'].
true.

?- likes(wallace, sheep).
false.

?- likes(grommit, cheese).
true.

?- friend(wallace, wallace).
false.

?- friend(grommit, wallace).
true.

?- friend(wallace, grommit).
true.


```