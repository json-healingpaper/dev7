fib(0, 0) :- !.
fib(1, 1) :- !.
fib(X, Result) :- X1 is X - 1, X2 is X - 2, fib(X1, Result1), fib(X2, Result2), Result is Result1 + Result2.


fib_seq(0, [0]) :- !.
fib_seq(1, [1, 0]) :- !.
fib_seq(X, [A, B, C | Tail]) :- Y is X - 1, fib_seq(Y, [B, C | Tail]), A is B + C.
