reverse(X, Result) :- do_reverse(X, [], Result).
do_reverse([Head | Tail], Temp, Result) :- do_reverse(Tail, [Head |Temp], Result).
do_reverse([], Result, Result) :- !.
