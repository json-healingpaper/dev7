min([Head | Tail], X) :- do_min([Head | Tail], Head, X).

do_min([Head | Tail], Min, Result) :- Head < Min, do_min(Tail, Head, Result).
do_min([Head | Tail], Min, Result) :- Head >= Min, do_min(Tail, Min, Result).
do_min([], Min, Min) :- !.
