hanoi(0, 0) :- !.
hanoi(1, 1) :- !.
hanoi(X, Result) :- X1 is X - 1, hanoi(X1, Result1), Result is Result1 * 2 + 1.
