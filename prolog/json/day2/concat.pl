concat([], List, List).
concat([Head | Tail1], List, [Head | Tail2]) :- concat(Tail1, List, Tail2).

% concat([1, 2], [3], What)
% concat([1 | 2], [3], [1 | Tail2-A]) :- concat([2], [3], Tail2-A)
% concat([2 | []], [3], [2 | Tail2-B]) :- concat([], [3], Tail2-B)
% concat([], [3], Tail2-B) :- concat([], [3], [3])
% Tail2-B = [3]
% Tail2-A = [2, 3]
% What = [1, 2, 3]