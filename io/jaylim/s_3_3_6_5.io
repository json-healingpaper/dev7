# 2차원 리스트를 위한 프로토타입을 작성하라. 
# dim(x, y) 메서드는 x개의 요소를 갖는 y번째 리스트를 할당해야 한다. 
# set(x, y, value)는 값을 설정하고, get(x, y)는 해당 좌표의 값을 리턴한다.

D2Array := List clone

D2Array dim := method(x, y, 
    tmp := D2Array clone
    for(i, 0, y - 1, 
        tmp append(list(nil, nil, nil))
    )
    tmp
)

D2Array set := method(x, y, value,
    self at(y) atPut(x, value)
)

D2Array get := method(x, y,
    self at(y) at(x)
)


d2 := D2Array dim(3, 3)
d2 println

d2 set(0, 0, 1)
d2 set(1, 0, 2)
d2 set(2, 0, 3)

d2 set(0, 1, "a")
d2 set(1, 1, "b")
d2 set(2, 1, "c")

d2 set(0, 2, 100)
d2 set(1, 2, 200)
d2 set(2, 2, 300)

d2 get(1, 2) println

d2 println