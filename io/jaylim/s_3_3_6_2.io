# 만약 나누는 값이 0이면 0을 리턴하도록 연산자 /의 구현을 바꾸어보라.

orgDiv := Number getSlot("/")

Number / := method(n, 
    if(n == 0, 0, self orgDiv(n))
)

(4 / 2) println
(4 / 0) println
(0 / 4) println