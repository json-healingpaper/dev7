# 피보나치 수열은 1에서 시작한다. 수열의 다음 수는 언제나 앞에 존재하는 두 수의 합이다. 1, 1, 2, 3, 5, 8, 13, 21, 등등 이런식이다. 
# n번째 피보나치 수를 찾는 프로그램을 작성하라. 예를 들어 fib(1)은 1이고 fib(4)는 3이다. 
# 보너스 점수를 받고 싶다면, 이 문제를 각각 재귀와 순차적 루프를 이용해서 풀어보라.

# 재귀
// fib := method(n, 
//     if(n <= 2, 
//         1, 
//         fib(n-2) + fib(n-1)
//     )
// )

# 순사적 루프
fib := method(n,
    f_2 := 1
    f_1 := 1
    f := 1
    for(i, 3, n, 
        f := f_2 + f_1
        f_2 = f_1
        f_1 = f
    )
    f
)

fib(1) println
fib(2) println
fib(3) println
fib(4) println
fib(5) println
fib(6) println
fib(7) println
fib(8) println
