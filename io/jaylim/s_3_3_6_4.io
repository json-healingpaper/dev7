# 리스트 슬롯에 myAverage라는 이름의 슬롯을 추가하라. 
# 리스트 안에 담긴 수의 평균을 계산하는 슬롯이다. 
# 리스트에 아무 수도 담겨 있지 않으면 어떤 일이 일어나야 하는가? 
# (보너스: 만약 리스트에 수가 아닌 값이 담겨 있으면 Io 예외를 발생시켜야 한다.)

List myAverage := method(
    if(self size == 0, return 0)

    sum := 0
    self foreach(v, 
        if(v type != "Number", Exception raise("not a Number"))
        sum = sum + v
    )

    sum / self size
)

// List myAverage := method(self average)

list(1, 2, 3, 4) myAverage println
list() myAverage println
list(1, 2, 3, 4, "Hello") myAverage println