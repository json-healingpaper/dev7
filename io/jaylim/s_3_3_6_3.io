# 2차원 배열에 담긴 수의 값을 모두 더하는 프로그램을 작성해보라.

array := list(
    list(1,2,3),
    list(4,5,6)
)

sumAll := method(array,
    sum := 0
    for(i, 0, array size - 1, 
        arr_at := array at(i)
        for(j, 0, arr_at size -1,
            sum = sum + (arr_at at(j))
        )
    )
    sum
)
sumAll(array) println

sumAll := method(array,
    sum := 0
    array foreach(a, a foreach(v, sum = sum + v))
)
sumAll(array) println