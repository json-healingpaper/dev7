Number orgDiv := Number getSlot("/")
Number / := method(n, if(n == 0, 0, self orgDiv(n)))

(1 / 0) println