List myAverage := method(
  if(self size == 0, return 0 / 0) // for Nan

  if(self select(x, x type != "Number") size != 0,
      Exception raise("not number elements"))

  (self sum) / (self size)
)

list() myAverage println
list(1, 2, 3, 4) myAverage println
list(1, 2, "3", 4) myAverage println