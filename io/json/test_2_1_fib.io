Fibonacci := Object clone
Fibonacci recur := method(n,
  if(n <= 1, n, Fibonacci recur(n - 1) + Fibonacci recur(n - 2))
)
Fibonacci loop := method(n,
  if (n <= 2, return 1)

  n0 := 1
  n1 := 1
  temp := 0

  for(i, 3, n,
    temp = n1 + n0
    n0 = n1
    n1 = temp
  )
  n1
)

Fibonacci recur(10) println
Fibonacci loop(10) println