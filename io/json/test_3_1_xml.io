Builder := Object clone
Builder depth := 0
Builder indent := method(
  self depth repeat (write("  "))
)
Builder forward := method(
  self indent
  writeln("<", call message name, ">") // <ul> <li>
  self depth = self depth + 1
  call message arguments foreach(arg,
    content := self doMessage(arg);
    if(content type == "Sequence", self indent; writeln(content)) // "Io"
  )
  self depth = self depth - 1
  self indent
  writeln("</", call message name, ">")
)

Builder ul(
  li("Io"),
  li("Lua"),
  li("Javascript")
)