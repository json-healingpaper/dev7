// mat_sum := method(mat, mat map(x, x sum) sum)
mat_sum := method(mat, mat flatten sum)

mat_sum(list(list(1, 2), list(3, 4))) println

