l := list(list(1, 2), list(3, 4))

fw := File with("mat.txt") remove openForUpdating
fw write(l map(i, i join(",")) join("\n"))
fw close

fr := File with("mat.txt") openForReading
mat := fr readLines() map(x, x split(",") map(asNumber))
mat println
