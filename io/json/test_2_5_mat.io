Mat := List clone
Mat sizeX := nil
Mat sizeY := nil

dim := method(x, y,
  lx := Mat clone

  x repeat(
    ly := list()
    y repeat(
      ly append(nil)
    )
    lx append(ly)
  )

  lx sizeX = x
  lx sizeY = y
  lx
)
Mat set := method(x, y, value,
  self at(x) atPut(y, value)
  self
)
Mat get := method(x, y,
  self at(x) at(y)
)
Mat transpose := method(
  sizeX := self sizeX
  sizeY := self sizeY

  trans := dim(self sizeY, self sizeX)
  trans println
  for(i, 0, sizeX - 1,
    for(j, 0, sizeY - 1,
      trans set(j, i, self get(i, j))
    )
  )
  trans
)

mat := dim(2, 2) println
mat sizeX println
mat sizeY println
mat set(0, 1, 1) println
mat get(0, 1) println
mat transpose println
