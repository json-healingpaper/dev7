unless := method(
  (call sender doMessage(call message argAt(0))) ifFalse(
   call sender doMessage(call message argAt(1))) ifTrue(
   call sender doMessage(call message argAt(2)))
)

// unless(1==2, write("One is not two|n"), write("one is two|n"))

Car := Object clone
Car problem := true

Checker := Object clone

Car check := method(Checker unless(problem, "Go!" println, "Don't go!" println))
Car check
