class Tree
  attr_accessor :children, :node_name
  
  def initialize(input)
    if input.is_a?(Hash) && input.keys.size == 1
      name, children_hash = input.to_a[0]
    elsif input.is_a?(Array)
      name = input[0]
      children_hash = input[1]
    end
    
    children = children_hash.map do |k, v| 
      Tree.new([k, v])
    end

    @node_name = name
    @children = children
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

input = {
  "grandpa" => {
    "dad" => {
      "child 1" => {}, "child 2" => {}
    },
    "uncle" => {
      "child 3" => {}, "child 4" => {}
    }
  }
}

tree = Tree.new(input)

puts "Visiting a node"
tree.visit {|node| puts node.node_name}
puts

puts "Visiting entrie tree"
tree.visit_all {|node| puts node.node_name}
