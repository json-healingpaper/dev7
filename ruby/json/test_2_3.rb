def grep(filename, str)
  line_num = 0

  File.open(filename, 'r') do |f|
    f.each_line do |l|
      if l.include?(str)
        puts "[line: #{line_num}] #{l}"
      end

      line_num += 1
    end
  end
end

grep("test_2_3.txt", "제이슨")