def is_it_right(ans, num)
  puts '정답보다 큽니다' if ans > num
  puts '정답이에요~!!' if ans == num
  puts '정답보다 작아요' if ans < num

  return ans == num
end

def guess(num) 
  right = false

  puts '0 ~ 9까지 숫자 중 하나를 맞춰보세요~!'
  while !right
    puts '숫자를 입력해주세요'
    ans = gets
    res = is_it_right(ans.to_i, num)
    right = res
  end  
end  

guess(rand(10))