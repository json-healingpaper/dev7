class ActsAsCsv
  def read
    file = File.new(self.class.to_s.downcase + ".txt")
    # https://docs.ruby-lang.org/en/2.6.0/String.html#method-i-chomp
    @headers = file.gets.chomp.split(", ")

    file.each do |row|
      # https://docs.ruby-lang.org/en/2.6.0/Array.html#class-Array-label-Adding+Items+to+Arrays
      @result << row.chomp.split(", ")
    end
  end

  def headers
    @headers
  end

  def csv_contents
    @result
  end

  # new 메서드가 호출되면 allocate 를 호출한 뒤에 initialize 메서드를 호출한다.
  def initialize
    @result = []
    read
  end
end

class RubyCsv < ActsAsCsv
end

m = RubyCsv.new
# https://docs.ruby-lang.org/en/2.6.0/Array.html#method-i-inspect
puts m.headers.inspect
puts m.csv_contents.inspect
