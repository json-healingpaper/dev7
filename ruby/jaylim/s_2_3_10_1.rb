# 코드 블록을 이용하거나 혹은 이용하지 않으면서 파일에 접근하는 방법을 찾아보라. 코드 블록의 장점은 무엇인가?
FILENAME = 'test.txt'

# 코드 블록 이용
File.open(FILENAME, 'r') {|f| puts f.read}

# 코드 블록 이용 안함
f = File.open(FILENAME, 'r')
puts f.read

# 코드 블록을 이용하면 코드가 간결해지고, 불필요한 변수를 선언하여 스코프를 더럽히지 않아도 된다.
