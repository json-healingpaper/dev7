# 해시를 어떻게 배열로 바꿀 것인가? 배열을 해시로도 바꿀 수 있는가?

hash = {1 => 'hi', 2 =>'hello'}
arr = hash.map {|k, v| v}
puts arr.to_s
arr = hash.map {|k, v| [k, v]}
puts arr.to_s
arr = hash.flatten
puts arr.to_s

puts '================================='

hash = arr.reduce(Hash.new) do |sum, n|
    # puts "sum: #{sum}, n: #{n}"
    sum[n] = n
    sum
end
puts hash.to_s
