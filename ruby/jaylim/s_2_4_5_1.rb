# CsvRow 객체를 리턴하는 each 메서드를 지원하도록 CSV 애플리케이션을 수정하라.
# 그 CsvRow 에서 주어진 헤딩 값에 해당하는 칼럼의 값을 리턴하도록 method_missing 을 사용하라.

module ActsAsCsv
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end

  module InstanceMethods
    def read
      @csv_contents = []
      filename = self.class.to_s.downcase + ".txt"
      file = File.new(filename)
      @headers = file.gets.chomp.split(", ")

      file.each do |row|
        @csv_contents << row.chomp.split(", ")
      end
    end

    def each(&block)
      @csv_contents.each { |row| block.call(CsvRow.new(@headers, row)) }
    end

    attr_accessor :headers, :csv_contents

    def initialize
      read
    end
  end
end

class CsvRow
  attr_accessor :headers, :row

  def initialize(headers, row)
    @headers = headers
    @row = row
  end

  def method_missing(name, *args)
    # to_s 를 붙여줘야 한다. name 은 Symbol class 임...
    # puts name.class : Symbol
    idx = @headers.index(name.to_s)
    @row[idx] if idx
  end
end

class RubyCsv
  include ActsAsCsv
  acts_as_csv
end

m = RubyCsv.new
puts m.headers.inspect
puts m.csv_contents.inspect
m.each { |row| puts row.one }
