class Roman
  def self.method_missing(name, *args)
    roman = name.to_s
    roman.gsub!("IV", "IIII")
    roman.gsub!("IX", "VIIII")
    roman.gsub!("XL", "XXXX")
    roman.gsub!("XC", "LXXXX")

    (roman.count("I") +
     roman.count("V") * 5 +
     roman.count("X") * 10 +
     roman.count("L") * 50 +
     roman.count("C") * 100)
  end

  # method_missing 말고 일반적으로 사용할 때
  def self.number_for(str)
    method_missing(str)
  end
end

puts Roman.X
puts Roman.XC
puts Roman.XII
puts Roman.X
puts Roman.number_for("X")
puts Roman.number_for("XII")
