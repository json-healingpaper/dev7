# 해시를 순차적으로 방문할 수 있는가?
# Hashes enumerate their values in the order that the corresponding keys were inserted.

hash = {2 => 'Ruby', 1 => 'Hello'}
hash.each {|key, val| puts "#{key} : #{val}"}
