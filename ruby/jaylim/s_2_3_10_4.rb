# 루비의 배열은 스택으로 사용할 수 있다. 배열은 흔한 자료구조 중 또 어떤 것으로 사용될 수 있는가?

# 스택: push, pop

arr = [1, 2, 3,'Hello', 4, 5, 'World', 6, 10]
puts arr.to_s

puts Array.methods.to_s